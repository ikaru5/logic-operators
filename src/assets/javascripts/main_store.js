import Persister from './persister'
import Solver from './solver'

class MainStore {

  constructor(mainComponent) {
    this._solver = new Solver()
    this._persister = new Persister("logicOperatorsPuzzle")
    this._component = mainComponent
    this.refs =
      {
        header: null
      }
    this._state = {}
    this.resetState()
  }

  getState() {
    return(this._state)
  }

  resetState(names = []) {
    this._state =
      {
        puzzle: {
          nodes: [],
          powerSources: [],
          sockets: [],
          targets: [],
          finalTarget: { positionX: 95, positionY: 45, power: false},
          connections: []
        },
        gameMode: false,
        reservedConnection: null,
        names: names,
        bruteForcing: false,
        solutions: [],
        bruteForceProgress: { total: 0, current: 0, foundWorking: 0}
      }
  }

  bruteForce() {
    if (this._state.bruteForcing) {
      this._solver.bruteForcing = false
      this.finishBruteForce("Brute Force cancelled")
    } else {
      this._state.solutions = []
      this._solver.buildUpStructure(this._state.puzzle)
      this._state.bruteForcing = true
      this._solver.initBruteForce()
      this._state.bruteForceProgress.total = this._solver.totalPossibilities
      this._state.bruteForceProgress.current = 0
      this.loadState()
      this.updateBruteForceProgress()
    }

  }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  async updateBruteForceProgress() {
    this._solver.bruteForcing = true
    await this.sleep(1000)
    while(this._solver.bruteForceStep()) {
      if (0 === this._solver.currentCombinationIndex % 100000) {
        this._state.bruteForceProgress.current = this._solver.currentCombinationIndex
        this._state.bruteForceProgress.foundWorking = this._solver.workingSolutions.length
        this.loadState()
        await this.sleep(1)
      }
    }
    this._solver.bruteForcing = false
    this._state.bruteForceProgress.foundWorking = this._solver.workingSolutions.length
    if (this._state.bruteForcing) this.finishBruteForce("Brute Force finsished")
  }

  async finishBruteForce(message) {
    this._state.bruteForcing = false
    let index = 1
    for (let solution of this._solver.workingSolutions) {
      this._state.solutions.push({ name: index, data: solution })
      index++
    }
    this.loadState()
    await this.sleep(1)
    alert(message)
  }

  loadSolution(data) {
    this._state.puzzle.nodes.forEach(function(node) {
      node.doppedToSocketId = undefined
    })

    let index = 0
    for (let socket of this._state.puzzle.sockets) {
      this.dropNodeToSocket(data[index], socket.id)
      index++
    }
    this.solve(false)
  }

  loadState() {
    this._state.reservedConnection = null
    this._component.setState(this._state)
  }

  switchToGameMode() {
    // detach all nodes
    for (let node of this._state.puzzle.nodes) {
      node.doppedToSocketId = undefined
    }
    this._state.gameMode = true
    this.loadState()
    this._solver.buildUpStructure(this._state.puzzle)
  }

  solve(finishGame = true) {
    this._solver.reset()
    this._solver.assignNodes(this._state.puzzle)
    let solvedTargets = this._solver.solve()
    for (let solvedTarget of solvedTargets) {
      if (0 === solvedTarget.id) {
        this._state.puzzle.finalTarget.power = solvedTarget.state
        if (finishGame) this.gameEnd()
      } else {
        for (let target of this._state.puzzle.targets) {
          if (target.id === solvedTarget.id) target.power = solvedTarget.state
        }
      }
    }
    this.loadState()
  }

  //----------------------- Connection Stuff

  gameEnd() {
    if (this._state.puzzle.finalTarget.power) alert("Spiel vorbei!")
  }

  getNextConnectionId() {
    let ids = this._state.puzzle.connections.map( (connectionData) => { return(connectionData.id) } )
    ids.push(0)

    return(Math.max(...ids) + 1)
  }

  reserveConnection(sourceType, sourceId, position, size) {
    this._state.reservedConnection =
      {
        out: { type: sourceType, id: sourceId, position: position, size: size }
      }
    this._component.setState(this._state)
  }

  getColorForNExtConnection() {
    let colors = [
      '800000', 'FF0000', 'FFA500', 'FFFF00', '808000', '008000', '800080', 'FF00FF',
      '00FF00', '008080', '00FFFF', '0000FF', '000080', '000000', '808080',
    ]
    return(colors[Math.floor(Math.random() * colors.length)]);
  }

  connect(targetType, targetId, targetPosition, size, targetInput = null, targetPositionType = "middle") {
    if (null !== this._state.reservedConnection) {
      let allreadyPresentConnection =
        this._state.puzzle.connections.find(function (connection) {
          return (
            this._state.reservedConnection.out.type === connection.out.type && this._state.reservedConnection.out.id === connection.out.id &&
            targetType === connection.in.type && targetId === connection.in.id && targetInput === connection.in.input
          )
        }.bind(this))
      // if the desired connection already exists, than remove just remove the old
      if (undefined !== allreadyPresentConnection) {
        this._state.puzzle.connections.splice(
          this._state.puzzle.connections.indexOf(allreadyPresentConnection),
          1
        )
      } else {
        this._state.puzzle.connections.push(
          {
            id: this.getNextConnectionId(),
            out: this._state.reservedConnection.out,
            color: this.getColorForNExtConnection(),
            in: { type: targetType, id: targetId, input: targetInput, position: targetPosition, positionType: targetPositionType, size: size }
          }
        )
      }
      this.loadState()
    }
    this._state.reservedConnection = null
  }

  removeConnectionsByOut(outId, outType) {
    this._state.puzzle.connections =
      this._state.puzzle.connections.filter(function(connection) {
        return !(outId === connection.out.id && outType === connection.out.type)
      })
    this.loadState()
  }

  removeConnectionsByIn(inId, inType, input = null) {
    this._state.puzzle.connections =
      this._state.puzzle.connections.filter(function(connection) {
        return !(inId === connection.in.id && inType === connection.in.type && input === connection.in.input)
      })
    this.loadState()
  }

  updateConnectionPosition(x, y, connectorType, connectorId, input = null) {
    for (let connection of this._state.puzzle.connections) {
      if (connectorType === connection.out.type && connectorId === connection.out.id) {
        connection.out.position.x = x
        connection.out.position.y = y
      }

      if (connectorType === connection.in.type && connectorId === connection.in.id && input === connection.in.input) {
        connection.in.position.x = x
        connection.in.position.y = y
      }
    }
  }

  //----------------------- Power Sources Stuff

  getNextPowerSourceId() {
    let ids = this._state.puzzle.powerSources.map( (powerSourceData) => { return(powerSourceData.id) } )
    ids.push(0)

    return(Math.max(...ids) + 1)
  }

  togglePowerSource(id, powerState) {
    this._state.puzzle.powerSources.forEach(function(powerSource) {
      if (id === powerSource.id) {
        powerSource.power = powerState
      }
    })
  }

  removePowerSource(id) {
    this._state.puzzle.powerSources =
      this._state.puzzle.powerSources.filter(function(powerSource) {
        return id !== powerSource.id
      })
    this.loadState()
  }


  addNewPowerSource() {
    this._state.puzzle.powerSources.push(
      {
        id: this.getNextPowerSourceId(),
        positionX: 5,
        positionY: 5,
        power: false
      }
    )
    this.loadState()
  }

  //----------------------- Node Stuff
  getNextNodeId() {
    let ids = this._state.puzzle.nodes.map( (nodeData) => { return(nodeData.id) } )
    ids.push(0)

    return(Math.max(...ids) + 1)
  }

  updateNodePosition(id, x, y) {
    this._state.puzzle.nodes.forEach(function(node) {
      if (id === node.id) {
        node.positionY = y
        node.positionX = x
      }
    })
  }

  updateNodeContent(id, content) {
    this._state.puzzle.nodes.forEach(function(node) {
      if (id === node.id) {
        node.content = content
      }
    })
    this.loadState()
  }

  dropNodeToSocket(nodeId, socketId) {
    this._state.puzzle.nodes.forEach(function(node) {
      if (nodeId === node.id) {
        node.doppedToSocketId = socketId
      }
    })

    if (this._state.gameMode) {
      this.solve()
    }
  }

  addNewNode() {
    this._state.puzzle.nodes.push(
      {
        id: this.getNextNodeId(),
        positionX: 5,
        positionY: 5,
        content: "a & b & c"
      }
    )
    this.loadState()
  }

  removeNode(id) {
    this._state.puzzle.nodes =
      this._state.puzzle.nodes.filter(function(node) {
        return id !== node.id
      })
    this.loadState()
  }

  //----------------------- Socket Stuff
  getNextSocketId() {
    let ids = this._state.puzzle.sockets.map( (socketData) => { return(socketData.id) } )
    ids.push(0)

    return(Math.max(...ids) + 1)
  }

  updateSocketPosition(id, x, y, bounds) {
    this._state.puzzle.sockets.forEach(function(socket) {
      if (id === socket.id) {
        socket.positionY = y
        socket.positionX = x
        socket.bounds = bounds
      }
    })
    this.loadState()
  }

  addNewSocket() {
    this._state.puzzle.sockets.push(
      {
        id: this.getNextSocketId(),
        positionX: 15,
        positionY: 15
      }
    )
    this.loadState()
  }

  getSocketById(id) {
    return(this._state.puzzle.sockets.find(function(socket) {
      return id === socket.id
    }))
  }

  removeSocket(id) {
    this._state.puzzle.sockets =
      this._state.puzzle.sockets.filter(function(socket) {
        return id !== socket.id
      })
    this.loadState()
  }

  //----------------------- Target Stuff
  getNextTargetId() {
    let ids = this._state.puzzle.targets.map( (targetData) => { return(targetData.id) } )
    ids.push(0)

    return(Math.max(...ids) + 1)
  }

  updateTargetPosition(id, x, y) {
    this._state.puzzle.targets.forEach(function(target) {
      if (id === target.id) {
        target.positionY = y
        target.positionX = x
      }
    })
    this.loadState()
  }

  addNewTarget() {
    this._state.puzzle.targets.push(
      {
        id: this.getNextTargetId(),
        positionX: 15,
        positionY: 15
      }
    )
    this.loadState()
  }

  removeTarget(id) {
    this._state.puzzle.targets =
      this._state.puzzle.targets.filter(function(target) {
        return id !== target.id
      })
    this.loadState()
  }

  //------------------------- Persistance Stuff

  loadPuzzle(puzzleData) {
    this.resetState(this._state.names)
    this.loadState()
    this._state.puzzle = puzzleData
    this.loadState()
    this._solver.buildUpStructure(this._state.puzzle)
  }

  persistState(name) {
    this._state.reservedConnection = null
    this._persister.save(this._state.puzzle, name)
  }

  finishPersistState() {
    this.fetchList()
  }

  fetchList() {
    this._persister.getList()
  }

  setNameList(names) {
    this._state.names = names;
    this._component.setState({names: names})
  }

}

export default MainStore

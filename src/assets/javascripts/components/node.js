import React, { Component } from "react";
import ReactDOM from "react-dom";

class Node extends React.Component {

  constructor(props) {
    super(props)
    this.reference = React.createRef();
    this.textarea = React.createRef();

    this.state = {
      moving: false,
      positionX: this.calculatePosition(),
      positionY: 10,
      editing: false,
      content: this.props.data.content
    }

    // bind events to component
    this.startMove = this.startMove.bind(this)
    this.move = this.move.bind(this)
    this.toggleEdit = this.toggleEdit.bind(this)
    this.remove = this.remove.bind(this)
    this.stopMove = this.stopMove.bind(this)
    this.renderEdit = this.renderEdit.bind(this)
    this.renderClose = this.renderClose.bind(this)
    this.onChangeTextContent = this.onChangeTextContent.bind(this)
  }

  calculatePosition() {
    let dist = 100 / (this.props.count + 1)
    let out = dist * (this.props.index + 1)
    return(out)
  }

  calculatePositionX() {
    if (this.state.moving || null === this.reference.current) return(this.state.positionX)
    if (undefined !== this.props.data.doppedToSocketId) {
      let socketData =
        document._store.getSocketById(this.props.data.doppedToSocketId)
      let totalWidth = this.reference.current.parentElement.offsetWidth
      let xPosPx =
        (socketData.bounds.x + (socketData.bounds.width / 2)) - // middlePoint of socket
        (this.reference.current.getBoundingClientRect().width / 2) // minus half of node
      return (
        xPosPx / totalWidth * 100
      )
    } else {
      return(this.calculatePosition())
    }
  }

  calculatePositionY() {
    if (this.state.moving || null === this.reference.current) return(this.state.positionY)
    if (undefined !== this.props.data.doppedToSocketId) {
      let socketData =
        document._store.getSocketById(this.props.data.doppedToSocketId)
      let totalHeight = this.reference.current.parentElement.offsetHeight + document._store.refs.header.current.offsetHeight
      let yPosPx =
        (socketData.bounds.y + (socketData.bounds.height / 2)) - // middlePoint of socket
        (this.reference.current.getBoundingClientRect().height / 2) // minus half of node
      return (
        yPosPx / totalHeight * 100
      )
    } else {
      return(10)
    }
  }

  wrapperStyle() {
    return(
      {
        left: this.calculatePositionX() + "%",
        top: this.calculatePositionY() + "%",
      }
    )
  }

  render() {
    return(
      <div style={this.wrapperStyle()} ref={this.reference} className="node-wrapper">
        <div
            className="node"
            onMouseDown={this.startMove}
            onTouchStart={this.startMove}
            onTouchMove={this.move}
            onMouseMove={this.move}
            onMouseUp={this.stopMove}
            onTouchEnd={this.stopMove}
        >
          {this.renderContent()}
        </div>
        {this.renderClose()}
        {this.renderEdit()}
      </div>
    )
  }

  renderClose() {
    if (!this.props.gameMode) {
      return (
        <div className="close material-icons" onClick={this.remove}>delete_forever</div>
      )
    }
  }

  renderEdit() {
    if (!this.props.gameMode) {
      return (
        <div className="editIcon material-icons" onClick={this.toggleEdit}>{this.state.editing ? "done" : "mode_edit"}</div>
      )
    }
  }

  renderContent() {
    if (this.state.editing) {
      return (
        <textarea onChange={this.onChangeTextContent} value={this.state.content} className="node-textarea" ref={this.textarea}/>
      )
    } else {
      return (
        <span className="centered-text">{this.props.data.content}</span>
      )
    }
  }

  onChangeTextContent(event) {
    this.setState({ content: event.target.value })
  }

  toggleEdit(event) {
    if (this.state.editing) {
      document._store.updateNodeContent(this.props.data.id, this.state.content)
      this.setState({ editing: false })
    } else {
      this.setState({ editing: true })
    }
  }

  move(event) {
    if (this.state.moving)
    {
      event.preventDefault()
      let mousePositionX = 0
      let mousePositionY = 0
      if ("touchmove" === event.type) {
        mousePositionX = event.touches[0].pageX
        mousePositionY = event.touches[0].pageY
      } else {
        mousePositionX = event.pageX
        mousePositionY = event.pageY
      }

      let target = this.reference.current
      // calculate left from mouse position and view width
      let left = mousePositionX - target.offsetWidth / 2
      left /= event.view.innerWidth
      left *= 100
      let widthAmount = target.offsetWidth / event.view.innerWidth * 100
      // should not be larger than widthAmount in %
      left = Math.min(100 - widthAmount, left)
      // should not be smaller than 0%
      left = Math.max(0, left)

      // calculate top from mouse position and view height
      let top = mousePositionY - target.offsetHeight / 2
      top /= event.view.innerHeight
      top *= 100
      let heightAmount = target.offsetHeight / event.view.innerHeight * 100
      // should not be larger than widthAmount in %
      top = Math.min(100.0 - heightAmount - 0.5, top)
      // should not be smaller than 5%
      let innerFrameStart =
        target.parentElement.offsetTop / event.view.innerHeight * 100
      top = Math.max(innerFrameStart, top)

      this.setState(
        {
          positionX: left,
          positionY: top
        }
      )

      // if mouse leaves view borders we should stop moving
      if (
        event.pageX <= 0 ||
        event.pageY <= 0 ||
        event.pageX >= event.view.innerWidth ||
        event.pageY >= event.view.innerHeight)
        {
          this.stopMove()
        }
    }
  }

  stopMove(event) {
    document.onmousemove = null
    let selfMiddlePoint = {
      x: this.reference.current.getBoundingClientRect().x + (this.reference.current.getBoundingClientRect().width / 2),
      y: this.reference.current.getBoundingClientRect().y + (this.reference.current.getBoundingClientRect().height / 2)
    }

    // drop to socket
    let dropToSocketId = undefined
    for (let socket of document._store._state.puzzle.sockets) {
      if (selfMiddlePoint.x > socket.bounds.x && selfMiddlePoint.x < (socket.bounds.x + socket.bounds.width) &&
        selfMiddlePoint.y > socket.bounds.y && selfMiddlePoint.y < (socket.bounds.y + socket.bounds.height)) {
        dropToSocketId = socket.id
      }
    }
    document._store.dropNodeToSocket(
      this.props.data.id,
      dropToSocketId
    )

    this.setState({
      moving: false,
      positionX: this.calculatePositionX(),
      positionY: this.calculatePositionY()
    })
  }

  startMove(event) {
    document.onmousemove = this.move
    this.setState({
      moving: true,
      positionX: this.calculatePositionX(),
      positionY: this.calculatePositionY()
    })
  }

  remove() {
    document._store.removeNode(this.props.data.id)
  }


}

export default Node;

import React, { Component } from "react";
import ReactDOM from "react-dom";

class Target extends React.Component {

  constructor(props) {
    super(props)
    this.reference = React.createRef();
    this.inRef = React.createRef();

    this.state = {
      moving: false,
      positionX: this.props.data.positionX,
      positionY: this.props.data.positionY
    }

    // bind events to component
    this.remove = this.remove.bind(this)
    this.connect = this.connect.bind(this)
    this.updateConnection = this.updateConnection.bind(this)
    this.resize = this.resize.bind(this)
    this.startMove = this.startMove.bind(this)
    this.move = this.move.bind(this)
    this.stopMove = this.stopMove.bind(this)
    this.renderClose = this.renderClose.bind(this)
  }

  componentDidMount() {
    window.addEventListener('resize', this.resize)
    this.updateConnection()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    this.updateConnection()
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resize)
  }

  wrapperStyle() {
    return(
      {
        left: (this.state.moving ? this.state.positionX : this.props.data.positionX) + "%",
        top: (this.state.moving ? this.state.positionY : this.props.data.positionY) + "%",
      }
    )
  }

  render() {
    return(
      <div style={this.wrapperStyle()} ref={this.reference} className="target-wrapper">
        <div className={"target " + (this.props.data.power ? "target-on" : "target-off")}
             onMouseDown={this.startMove}
             onTouchStart={this.startMove}
             onTouchMove={this.move}
             onMouseMove={this.move}
             onMouseUp={this.stopMove}
             onTouchEnd={this.stopMove}
        />
        {this.renderClose()}
        <div ref={this.inRef} className="connector connector-in" onClick={this.connect}/>
      </div>
    )
  }

  renderClose() {
    if (!this.props.gameMode) {
      return (
        <div className="close material-icons" onClick={this.remove}>delete_forever</div>
      )
    }
  }

  move(event) {
    if (this.state.moving)
    {
      let mousePositionX = 0
      let mousePositionY = 0
      if ("touchmove" === event.type) {
        mousePositionX = event.touches[0].pageX
        mousePositionY = event.touches[0].pageY
      } else {
        mousePositionX = event.pageX
        mousePositionY = event.pageY
      }

      let target = this.reference.current
      // calculate left from mouse position and view width
      let left = mousePositionX - target.offsetWidth / 2
      left /= event.view.innerWidth
      left *= 100
      let widthAmount = target.offsetWidth / event.view.innerWidth * 100
      // should not be larger than widthAmount in %
      left = Math.min(100 - widthAmount, left)
      // should not be smaller than 0%
      left = Math.max(0, left)

      // calculate top from mouse position and view height
      let top = mousePositionY - target.offsetHeight / 2
      top /= event.view.innerHeight
      top *= 100
      let heightAmount = target.offsetHeight / event.view.innerHeight * 100
      // should not be larger than widthAmount in %
      top = Math.min(100.0 - heightAmount - 0.5, top)
      // should not be smaller than 5%
      let innerFrameStart =
        target.parentElement.offsetTop / event.view.innerHeight * 100
      top = Math.max(innerFrameStart, top)

      this.setState(
        {
          positionX: left,
          positionY: top
        }
      )

      // if mouse leaves view borders we should stop moving
      if (
        event.pageX <= 0 ||
        event.pageY <= 0 ||
        event.pageX >= event.view.innerWidth ||
        event.pageY >= event.view.innerHeight)
      {
        this.stopMove()
      }
    }
  }

  stopMove(event) {
    if (this.props.gameMode) return
    document.onmousemove = null
    document._store.updateTargetPosition(
      this.props.data.id,
      this.state.positionX,
      this.state.positionY
    )
    this.setState({ moving: false })
  }

  startMove(event) {
    if (this.props.gameMode) return
    if (!this.state.moving) {
      document.onmousemove = this.move
      this.setState(
        {
          moving: true,
          positionX: this.props.data.positionX,
          positionY: this.props.data.positionY
        }
      )
    }
  }

  remove() {
    document._store.removeConnectionsByIn(this.props.data.id, "target")
    document._store.removeTarget(this.props.data.id)
  }

  // ---------------------- CONNECTION STUFF

  connect(event) {
    document._store.connect(
      "target",
      this.props.data.id,
      this.inRef.current.getBoundingClientRect(),
      { width: this.inRef.current.offsetWidth, height: this.inRef.current.offsetHeight },
    )
  }

  resize() {
    this.updateConnection()
    document._store.loadState()
  }

  updateConnection() {
    document._store.updateConnectionPosition(
      this.inRef.current.getBoundingClientRect().x,
      this.inRef.current.getBoundingClientRect().y,
      "target",
      this.props.data.id
    )
  }
}

export default Target

import React, { Component } from "react";
import ReactDOM from "react-dom";

class FinalTarget extends React.Component {

  constructor(props) {
    super(props)
    this.inRef = React.createRef()

    // bind events to component
    this.connect = this.connect.bind(this)
    this.updateConnection = this.updateConnection.bind(this)
    this.resize = this.resize.bind(this)
  }

  componentDidMount() {
    window.addEventListener('resize', this.resize)
    this.updateConnection()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    this.updateConnection()
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resize)
  }

  wrapperStyle() {
    return(
      {
        left: this.props.data.positionX + "%",
        top: this.props.data.positionY + "%",
      }
    )
  }

  render() {
    return(
      <div className="final-target-wrapper" style={this.wrapperStyle()}>
        <div className={"final-target " + (this.props.data.power ? "final-target-on" : "final-target-off")} />
        <div ref={this.inRef} className="connector connector-in" onClick={this.connect}/>
      </div>
    );
  }

  // ---------------------- CONNECTION STUFF

  connect(event) {
    document._store.connect(
      "finalTarget",
      1,
      this.inRef.current.getBoundingClientRect(),
      { width: this.inRef.current.offsetWidth, height: this.inRef.current.offsetHeight },
    )
  }

  resize() {
    this.updateConnection()
    document._store.loadState()
  }

  updateConnection() {
    document._store.updateConnectionPosition(
      this.inRef.current.getBoundingClientRect().x,
      this.inRef.current.getBoundingClientRect().y,
      "finalTarget",
      1
    )
  }
}

export default FinalTarget

import React, { Component } from "react";
import ReactDOM from "react-dom";
import Header from './header';
import InnerFrame from './inner_frame';
import MainStore from '../main_store';

class OuterFrame extends React.Component {

  constructor(props) {
    super(props);
    document._store = new MainStore(this);
    this.state = document._store.getState();
  }

  render() {
    return(
      <div className="outer-frame">
        <Header
          names={this.state.names}
          gameMode={this.state.gameMode}
          bruteForcing={this.state.bruteForcing}
          solutions={this.state.solutions}
          bruteForceProgress={this.state.bruteForceProgress}
        />
        <InnerFrame puzzle={this.state.puzzle} gameMode={this.state.gameMode} currentReserved={this.state.reservedConnection} />
      </div>
    );
  }
}

export default OuterFrame;

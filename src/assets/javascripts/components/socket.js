import React, { Component } from "react";
import ReactDOM from "react-dom";

class Socket extends React.Component {

  constructor(props) {
    super(props)
    this.reference = React.createRef()
    this.outRef = React.createRef()
    this.aRef = React.createRef()
    this.bRef = React.createRef()
    this.cRef = React.createRef()

    this.state = {
      moving: false,
      positionX: this.props.data.positionX,
      positionY: this.props.data.positionY
    }

    // bind events to component
    this.remove = this.remove.bind(this)
    this.startMove = this.startMove.bind(this)
    this.move = this.move.bind(this)
    this.stopMove = this.stopMove.bind(this)
    this.initConnect = this.initConnect.bind(this)
    this.connect = this.connect.bind(this)
    this.updateConnections = this.updateConnections.bind(this)
    this.resize = this.resize.bind(this)
    this.renderCloseButton = this.renderCloseButton.bind(this)
  }

  componentDidMount() {
    window.addEventListener('resize', this.resize)
    document._store.updateSocketPosition(
      this.props.data.id,
      this.state.positionX,
      this.state.positionY,
      this.reference.current.getBoundingClientRect()
    )
    this.updateConnections()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    this.updateConnections()
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resize)
  }

  wrapperStyle() {
    return(
      {
        left: (this.state.moving ? this.state.positionX : this.props.data.positionX) + "%",
        top: (this.state.moving ? this.state.positionY : this.props.data.positionY) + "%",
      }
    )
  }

  outStyle() {
    let isReservedConnectionMYself =
      (null !== this.props.currentReserved && this.props.currentReserved.out.id === this.props.data.id && "socket" === this.props.currentReserved.out.type)
    return(
      {
        boxShadow: isReservedConnectionMYself ? 'rgba(0, 0, 0, 0.2) 0px 4px 8px 0px, rgba(255, 129, 0, 0.74) 0 0 5px 4px' : ''
      }
    )
  }

  render() {
    return(
      <div style={this.wrapperStyle()} ref={this.reference} className="socket-wrapper">
        <div
            className="socket"
            onMouseDown={this.startMove}
            onTouchStart={this.startMove}
            onTouchMove={this.move}
            onMouseMove={this.move}
            onMouseUp={this.stopMove}
            onTouchEnd={this.stopMove}
        >
          {this.props.data.form}
        </div>
        {this.renderCloseButton()}
        <div ref={this.aRef} className="connector connector-a bottom-connector" letter="a" onClick={this.connect}>
          <span letter="a" className="a-label">a</span>
        </div>
        <div ref={this.bRef} className="connector connector-b" letter="b" onClick={this.connect}>
          <span letter="b" className="b-label">b</span>
        </div>
        <div ref={this.cRef} className="connector connector-c top-connector" letter="c" onClick={this.connect}>
          <span letter="c" className="c-label">c</span>
        </div>
        <div ref={this.outRef} style={this.outStyle()} className="connector connector-d" onClick={this.initConnect}/>
      </div>
    );
  }

  renderCloseButton() {
    if (!this.props.gameMode) {
      return (
        <div className="close material-icons" onClick={this.remove}>delete_forever</div>
      )
    }
  }

  move(event) {
    if (this.state.moving)
    {
      let mousePositionX = 0
      let mousePositionY = 0
      if ("touchmove" === event.type) {
        mousePositionX = event.touches[0].pageX
        mousePositionY = event.touches[0].pageY
      } else {
        mousePositionX = event.pageX
        mousePositionY = event.pageY
      }

      let target = this.reference.current
      // calculate left from mouse position and view width
      let left = mousePositionX - target.offsetWidth / 2
      left /= event.view.innerWidth
      left *= 100
      let widthAmount = target.offsetWidth / event.view.innerWidth * 100
      // should not be larger than widthAmount in %
      left = Math.min(100 - widthAmount, left)
      // should not be smaller than 0%
      left = Math.max(0, left)

      // calculate top from mouse position and view height
      let top = mousePositionY - target.offsetHeight / 2
      top /= event.view.innerHeight
      top *= 100
      let heightAmount = target.offsetHeight / event.view.innerHeight * 100
      // should not be larger than widthAmount in %
      top = Math.min(100.0 - heightAmount - 0.5, top)
      // should not be smaller than 5%
      let innerFrameStart =
        target.parentElement.offsetTop / event.view.innerHeight * 100
      top = Math.max(innerFrameStart, top)

      this.setState(
        {
          positionX: left,
          positionY: top
        }
      )

      // if mouse leaves view borders we should stop moving
      if (
        event.pageX <= 0 ||
        event.pageY <= 0 ||
        event.pageX >= event.view.innerWidth ||
        event.pageY >= event.view.innerHeight)
        {
          this.stopMove()
        }
    }
  }

  stopMove(event) {
    if (this.props.gameMode) return
    document.onmousemove = null
    document._store.updateSocketPosition(
      this.props.data.id,
      this.state.positionX,
      this.state.positionY,
      this.reference.current.getBoundingClientRect()
    )
    this.setState({ moving: false })
  }

  startMove(event) {
    if (this.props.gameMode) return
    document.onmousemove = this.move
    this.setState(
      {
          moving: true,
          positionX: this.props.data.positionX,
          positionY: this.props.data.positionY
        }
      )
  }

  remove() {
    document._store.removeConnectionsByOut(this.props.data.id, "socket")
    document._store.removeConnectionsByIn(this.props.data.id, "socket", "a")
    document._store.removeConnectionsByIn(this.props.data.id, "socket", "b")
    document._store.removeConnectionsByIn(this.props.data.id, "socket", "c")
    document._store.removeSocket(this.props.data.id)
  }

  // ---------------------- CONNECTION STUFF

  initConnect() {
    if (this.props.gameMode) return
    document._store.reserveConnection(
      "socket",
      this.props.data.id,
      this.outRef.current.getBoundingClientRect(),
      { width: this.outRef.current.offsetWidth, height: this.outRef.current.offsetHeight }
    )
  }

  connect(event) {
    if (this.props.gameMode) return
    let letter = event.target.attributes.letter.value
    switch (letter) {
      case "a":
        document._store.connect(
          "socket",
          this.props.data.id,
          this.aRef.current.getBoundingClientRect(),
          { width: this.aRef.current.offsetWidth, height: this.aRef.current.offsetHeight },
          letter,
          "bottom"
        )
        break
      case "b":
        document._store.connect(
          "socket",
          this.props.data.id,
          this.bRef.current.getBoundingClientRect(),
          { width: this.bRef.current.offsetWidth, height: this.bRef.current.offsetHeight },
          letter
        )
        break
      case "c":
        document._store.connect(
          "socket",
          this.props.data.id,
          this.cRef.current.getBoundingClientRect(),
          { width: this.cRef.current.offsetWidth, height: this.cRef.current.offsetHeight },
          letter,
          "top"
        )
        break
    }
  }

  resize() {
    this.updateConnections()
    document._store.updateSocketPosition(
      this.props.data.id,
      this.state.positionX,
      this.state.positionY,
      this.reference.current.getBoundingClientRect()
    )
    document._store.loadState()
  }

  updateConnections() {
    document._store.updateConnectionPosition(
      this.aRef.current.getBoundingClientRect().x,
      this.aRef.current.getBoundingClientRect().y,
      "socket",
      this.props.data.id,
      "a"
    )

    document._store.updateConnectionPosition(
      this.bRef.current.getBoundingClientRect().x,
      this.bRef.current.getBoundingClientRect().y,
      "socket",
      this.props.data.id,
      "b"
    )

    document._store.updateConnectionPosition(
      this.cRef.current.getBoundingClientRect().x,
      this.cRef.current.getBoundingClientRect().y,
      "socket",
      this.props.data.id,
      "c"
    )

    document._store.updateConnectionPosition(
      this.outRef.current.getBoundingClientRect().x,
      this.outRef.current.getBoundingClientRect().y,
      "socket",
      this.props.data.id
    )
  }

}

export default Socket;

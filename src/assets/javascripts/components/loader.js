import React, { Component } from "react";
import ReactDOM from "react-dom";

class Loader extends React.Component {

  constructor(props) {
    super(props)
    this.saveNameRef = React.createRef()
    this.selectNameRef = React.createRef()

    this.state = {
      mode: "idle"
    }

    // bind events to component
    this.load = this.load.bind(this)
    this.initLoad = this.initLoad.bind(this)
    this.save = this.save.bind(this)
    this.initSave = this.initSave.bind(this)
  }



  render() {
    switch(this.state.mode){
      case "loading":
        return(
          <div className="loader">
            <span>Names: </span>
            <select className="loader-select" name="names" ref={this.selectNameRef}>
              {
                this.props.names.map(
                  (nameData, i) => <option key={i} value={JSON.stringify(nameData.data)}>{nameData.name}</option>
                )
              }
            </select>
            <button onClick={this.load}>Load</button>
            <button onClick={() => this.setState({ mode: "idle" })}>Cancel</button>
          </div>
        )
      case "saving":
        return(
          <div className="loader">
            Name:
            <input className="loader-input" type="text" ref={this.saveNameRef}/>
            <button onClick={this.save}>Save</button>
            <button onClick={() => this.setState({ mode: "idle" })}>Cancel</button>
          </div>
        )
      default:
        return(
          <div className="loader">
            <button onClick={this.initSave}>Save</button>
            <button onClick={this.initLoad}>Load</button>
          </div>
        )
    }
  }

  load() {
    let select = this.selectNameRef.current
    let state = JSON.parse(select.options[select.selectedIndex].value)
    document._store.loadPuzzle(state)
    this.setState({mode: "idle"})
  }

  save() {
    if (this.saveNameRef.current.value.length > 0) {
      document._store.persistState(this.saveNameRef.current.value)
      this.setState({mode: "idle"})
    } else {
      alert("Bitte geben Sie einen Namen ein!")
    }

  }

  initLoad() {
    this.setState({mode: "loading"})
  }

  initSave() {
    this.setState({mode: "saving"})
  }

}

export default Loader

import React, { Component } from "react";
import ReactDOM from "react-dom";
import Node from "./node"
import Socket from "./socket"
import Target from "./target"
import FinalTarget from "./final_target"
import PowerSource from "./power_source"
import Connection from "./connection"

class InnerFrame extends React.Component {

  render() {
    return(
      <div className="inner-frame">
        {
          this.props.puzzle.nodes.map(
            (nodeData, i) => <Node key={i} index={i} data={nodeData} count={this.props.puzzle.nodes.length} gameMode={this.props.gameMode} />
          )
        }
        {
          this.props.puzzle.sockets.map(
            (socketData, i) => <Socket key={i} data={socketData} currentReserved={this.props.currentReserved} gameMode={this.props.gameMode} />
          )
        }
        {
          this.props.puzzle.powerSources.map(
            (powerSourceData, i) => <PowerSource key={i} index={i} data={powerSourceData} count={this.props.puzzle.powerSources.length} currentReserved={this.props.currentReserved} gameMode={this.props.gameMode} />
          )
        }
        {
          this.props.puzzle.targets.map(
            (targetData, i) => <Target key={i} index={i} data={targetData} gameMode={this.props.gameMode} />
          )
        }
        <FinalTarget data={this.props.puzzle.finalTarget} />

        <svg className="svg-container" id={"svg1"} width="100%" height="100%" >
          {
            this.props.puzzle.connections.map(
              (connectionData, i) => <Connection key={i} index={i} data={connectionData} />
            )
          }
        </svg>
      </div>
    );
  }
}

export default InnerFrame;

import React, { Component } from "react";
import ReactDOM from "react-dom";
import OuterFrame from './outer_frame';
import MainStore from '../main_store';

class Application {

  start() {
    ReactDOM.render(
      <OuterFrame />,
      document.getElementById('app')
    );
  }

}

export default Application;

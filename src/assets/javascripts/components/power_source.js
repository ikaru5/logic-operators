import React, { Component } from "react";
import ReactDOM from "react-dom";

class PowerSource extends React.Component {

  constructor(props) {
    super(props)
    this.reference = React.createRef();
    this.outRef = React.createRef();

    // bind events to component
    this.toggle = this.toggle.bind(this)
    this.renderCloseButton = this.renderCloseButton.bind(this)
    this.remove = this.remove.bind(this)
    this.initConnect = this.initConnect.bind(this)
    this.updateConnection = this.updateConnection.bind(this)
    this.resize = this.resize.bind(this)
    this.state = { oldPosition: this.calculatePosition() }
  }

  componentDidMount() {
    window.addEventListener('resize', this.resize)
    this.updateConnection()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    this.updateConnection()
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resize)
  }

  calculatePosition() {
    let dist = 100 / (this.props.count + 1)
    let out = dist * (this.props.index + 1)
    return(out)
  }

  outStyle() {
    let isReservedConnectionMYself =
      (null !== this.props.currentReserved && this.props.currentReserved.out.id === this.props.data.id && "powerSource" === this.props.currentReserved.out.type)
    return(
      {
        boxShadow: isReservedConnectionMYself ? 'rgba(0, 0, 0, 0.2) 0px 4px 8px 0px, rgba(255, 129, 0, 0.74) 0 0 5px 4px' : ''
      }
    )
  }

  render() {
    return(
      <div style={{ top: this.calculatePosition() + "%" }} className="power-source-wrapper">
        <div className={"power-source " + (this.props.data.power ? "power-source-on" : "power-source-off")} onClick={this.toggle} />
        {this.renderCloseButton()}
        <div ref={this.outRef} style={this.outStyle()} className="connector connector-out" onClick={this.initConnect}/>
      </div>
    )
  }

  renderCloseButton() {
    if (!this.props.gameMode) {
      return (
        <div className="close material-icons" onClick={this.remove}>delete_forever</div>
      )
    }
  }

  toggle() {
    if (this.props.gameMode) return
    document._store.togglePowerSource(this.props.data.id, !this.props.data.power)
    this.forceUpdate()
  }

  remove() {
    document._store.removeConnectionsByOut(this.props.data.id, "powerSource")
    document._store.removePowerSource(this.props.data.id)
  }

  // ---------------------- CONNECTION STUFF

  initConnect() {
    if (this.props.gameMode) return
    document._store.reserveConnection(
      "powerSource",
      this.props.data.id,
      this.outRef.current.getBoundingClientRect(),
      { width: this.outRef.current.offsetWidth, height: this.outRef.current.offsetHeight }
    )
  }

  resize() {
    this.updateConnection()
    document._store.loadState()
  }

  updateConnection() {
    document._store.updateConnectionPosition(
      this.outRef.current.getBoundingClientRect().x,
      this.outRef.current.getBoundingClientRect().y,
      "powerSource",
      this.props.data.id
    )

    // if updated position we need to set global state
    if (this.calculatePosition() !== this.state.oldPosition) {
      this.setState({ oldPosition: this.calculatePosition() })
      document._store.loadState()
    }
  }
}

export default PowerSource;

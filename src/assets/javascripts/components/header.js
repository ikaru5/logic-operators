import React, { Component } from "react";
import ReactDOM from "react-dom";
import Loader from "./loader";

class Header extends React.Component {

  constructor() {
    super()
    this.reference = React.createRef()
    this.solutionSelect = React.createRef()
    document._store.refs.header = this.reference

    this.renderGameMasterElements = this.renderGameMasterElements.bind(this)
    this.loadSolution = this.loadSolution.bind(this)
  }

  render() {
    return(
      <div ref={this.reference} className="header">
        {this.renderGameMasterElements()}
      </div>
    );
  }

  renderGameMasterElements() {
    if (!this.props.gameMode) {
      return (
        <span>
          <button className="spawn-button" onClick={this.switchToGameMode}>Switch To GameMode</button>
          <button className="spawn-button" onClick={this.spawnPowerSource}>Add PowerSource</button>
          <button className="spawn-button" onClick={this.spawnNode}>Add Node</button>
          <button className="spawn-button" onClick={this.spawnSocket}>Add Socket</button>
          <button className="spawn-button" onClick={this.spawnTarget}>Add Target</button>
          <button className="spawn-button" onClick={this.bruteForce}>{this.props.bruteForcing ? "Stop BF" : "Start BF"}</button>
          {this.renderBruteForce()}
          <Loader names={this.props.names} />
        </span>
      )
    }
  }

  renderBruteForce() {
    if (this.props.bruteForcing) {
      return(
        <span>{this.props.bruteForceProgress.current + " of " + this.props.bruteForceProgress.total + " - Found Working: " + this.props.bruteForceProgress.foundWorking}</span>
      )
    } else {
      if (0 < this.props.solutions.length) {
        return(
          <span>
            <select ref={this.solutionSelect} name="solutions">
              {
                this.props.solutions.map(
                  (solutionData, i) => <option key={i} value={JSON.stringify(solutionData.data)}>{solutionData.name}</option>
                )
              }
            </select>
            <button className="spawn-button" onClick={this.loadSolution}>Load Solution</button>
            <span>{"Found Working: " + this.props.bruteForceProgress.foundWorking}</span>
          </span>
        )
      }
    }
  }

  loadSolution() {
    let selected = this.solutionSelect.current.options[this.solutionSelect.current.selectedIndex]
    document._store.loadSolution(JSON.parse(selected.value))
  }

  bruteForce() {
    document._store.bruteForce()
  }

  switchToGameMode() {
    document._store.switchToGameMode()
  }

  spawnNode() {
    document._store.addNewNode()
  }

  spawnSocket() {
    document._store.addNewSocket()
  }

  spawnPowerSource() {
    document._store.addNewPowerSource()
  }

  spawnTarget() {
    document._store.addNewTarget()
  }

}

export default Header

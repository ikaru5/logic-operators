import React, { Component } from "react";
import ReactDOM from "react-dom";

class Connection extends React.Component {

  constructor(props) {
    super(props)
  }

  render() {
    return(
      <path
        id={"myNewPath" + this.props.data.id}
        d={this.getSVGPath()}
        strokeWidth="0.3em"
        style={{stroke: this.props.data.color, fill: 'none' }}
        />
    );
  }

  getSVGPath() {
    let headerHeight = document._store.refs.header.current.offsetHeight
    let outHash = {
      x: this.props.data.out.position.x + this.props.data.out.size.width / 2,
      y: this.props.data.out.position.y + this.props.data.out.size.height / 2 - headerHeight
    }

    let inHash = {
      x: this.props.data.in.position.x + this.props.data.in.size.width / 2,
      y: this.props.data.in.position.y + this.props.data.in.size.height / 2 - headerHeight,
      connectorType: this.props.data.in.positionType
    }

    return(
      this.drawPath(
        outHash.x,
        outHash.y,
        inHash.x,
        inHash.y,
        inHash.connectorType
      )
    )
  }

  drawPath(startX, startY, endX, endY, type) {
    let deltaX = (endX - startX)
    let deltaY = (endY - startY)

    // for further calculations
    let deltaXMultiplier = 0.1

    let startCurveX = (startX + (deltaX * deltaXMultiplier))
    let startCurveY = startY
    // end position may be on top or bottom of a socket, need correction for curve
    let correction = 0
    let curve = ""
    let getCurveLineString = function (c1x, c1y, c2x, c2y, endX, endY) {
      return (" C" + c1x + " " +  c1y  + ", " + c2x + " " + c2y + ", " + endX + " " + endY)
    }

    switch (type) {
      case "top":
        correction = deltaY * 0.5
        correction *= startY > endY ? -1 : 1
        curve =
          getCurveLineString(
            (endX - (deltaX * deltaXMultiplier)),
            startY,
            startX,
            endY - correction,
            endX,
            endY
          )
        break
      case "bottom":
        correction = deltaY * 0.5
        correction *= startY > endY ? -1 : 1
        curve =
          getCurveLineString(
            (endX - (deltaX * deltaXMultiplier)),
            startY,
            startX,
            endY + correction,
            endX,
            endY
          )
        break
      default:
        curve =
          getCurveLineString(
            (endX - (deltaX * deltaXMultiplier)),
            startY,
            (startX + (deltaX * deltaXMultiplier)),
            endY,
            endX,
            endY
          )
    }

    return(
      "M"  + startX + " " + startY +
      " H" + startCurveX +
      curve
      // " L" + endX + " " + endY
    );
  }

}

export default Connection;

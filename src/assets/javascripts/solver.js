class Socket {
  constructor(id, solver) {
    this.id = id
    this.a = []
    this.b = []
    this.c = []
    this.aState = -1
    this.bState = -1
    this.cState = -1
    this.outState = -1
    this.node = undefined

    this.setRef = this.setRef.bind(this)
    this.getOutState = this.getOutState.bind(this)
    this.reset = this.reset.bind(this)
  }

  reset() {
    this.aState = -1
    this.bState = -1
    this.cState = -1
    this.outState = -1
    this.node = undefined
  }

  getOutState() {
    if (undefined !== this.node) {
      if (-1 === this.outState) {
        // calculate a state
        if (-1 === this.aState) {
          this.aState = false
          if (0 !== this.a.length) {
            for (let input of this.a) {
              this.aState = this.aState || input.getOutState()
            }
          }
        }

        // calculate b state
        if (-1 === this.bState) {
          this.bState = false
          if (0 !== this.b.length) {
            for (let input of this.b) {
              this.bState = this.bState || input.getOutState()
            }
          }
        }

        // calculate c state
        if (-1 === this.cState) {
          this.cState = false
          if (0 !== this.c.length) {
            for (let input of this.c) {
              this.cState = this.cState || input.getOutState()
            }
          }
        }

        this.outState = this.node.func(this.aState, this.bState, this.cState)
        // some weird js fuck up
        if (1 === this.outState) this.outState = true
        if (0 === this.outState) this.outState = false

        // makes sense for me! but not necessary
        if (!this.aState && !this.bState && !this.cState) this.outState = false
      }
    } else {
      this.outState = false
    }

    return (this.outState)
  }

  setRef(letter, ref) {
    switch (letter) {
      case "a":
        this.a.push(ref)
        break
      case "b":
        this.b.push(ref)
        break
      case "c":
        this.c.push(ref)
        break
    }
  }

}

class Node {
  constructor(id, content, solver) {
    this.id = id
    this.content = "this.func = function s(a,b,c) { return(" + content + ") }"
    this.func = eval(this.content)
    this.solver = solver
  }
}

class Target {
  constructor(id, solver) { // id == 0 -> finalTarget
    this.id = id
    this.state = false
    this.in = []
    this.solver = solver
  }

  updateState() {
    if (0 !== this.in.length) {
      this.state = false
      for (let input of this.in) {
        this.state = this.state || input.getOutState()
      }
    }
  }
}

class PowerSource {
  constructor(id, state, solver) {
    this.id = id
    this.state = state
    this.solver = solver

    this.getOutState = this.getOutState.bind(this)
  }

  getOutState() {
    return (this.state)
  }
}

class Solver {

  constructor() {
    this.buildUpStructure = this.buildUpStructure.bind(this)
    this.resetStructure = this.resetStructure.bind(this)
    this.getElement = this.getElement.bind(this)
    this.assignNodes = this.assignNodes.bind(this)
    this.solve = this.solve.bind(this)
    this.initBruteForce = this.initBruteForce.bind(this)
    this.bruteForceStep = this.bruteForceStep.bind(this)
    this.reset = this.reset.bind(this) // need to be run before solving again
    this.resetStructure()
  }

  resetStructure() {
    this.sockets = {}
    this.socketsArray = []
    this.nodes = {}
    this.nodesArray = []
    this.targets = {}
    this.targetsArray = []
    this.powerSources = {}
  }

  buildUpStructure(puzzle) {
    this.resetStructure()

    // build up powerSources
    for (let ps of puzzle.powerSources) {
      this.powerSources[ps.id] = new PowerSource(ps.id, ps.power, this)
    }

    // build up targets and finalTarget
    for (let t of puzzle.targets) {
      this.targets[t.id] = new Target(t.id, this)
      this.targetsArray.push(this.targets[t.id])
    }
    this.targets[0] = new Target(0, this)
    this.targetsArray.push(this.targets[0])

    // build up sockets
    for (let s of puzzle.sockets) {
      this.sockets[s.id] = new Socket(s.id, this)
      this.socketsArray.push(this.sockets[s.id])
    }

    // build up nodes
    for (let n of puzzle.nodes) {
      this.nodes[n.id] = new Node(n.id, n.content, this)
      this.nodesArray.push(this.nodes[n.id])
    }

    // now connect everything
    for (let c of puzzle.connections) {
      switch (c.in.type) {
        case "socket":
          this.sockets[c.in.id].setRef(c.in.input, this.getElement(c.out.type, c.out.id))
          break
        case "target":
          this.targets[c.in.id].in.push(this.getElement(c.out.type, c.out.id))
          break
        case "finalTarget":
          this.targets[0].in.push(this.getElement(c.out.type, c.out.id))
          break
        default:
          // shit happened -> need debug
          debugger
      }
    }
  }

  assignNodes(puzzle) {
    for (let n of puzzle.nodes) {
      if (undefined !== this.sockets[n.doppedToSocketId]) this.sockets[n.doppedToSocketId].node = this.nodes[n.id]
    }
  }

  getElement(type, id) {
    switch (type) {
      case "powerSource":
        return (this.powerSources[id])
      case "socket":
        return (this.sockets[id])
      case "node":
        return (this.nodes[id])
      default:
        // shit happened -> need debug
        debugger
    }
  }

  reset() {
    for (let socket of this.socketsArray) {
      socket.reset()
    }
  }

  solve() {
    for (let target of this.targetsArray) {
      target.updateState()
    }
    return this.targetsArray
  }

  initBruteForce() {
    let possibleNodesCount = this.nodesArray.length + 1
    let availableSocketsCount = this.socketsArray.length
    let fak = function fak(x) {
      return x * (x > 1 ? fak(x - 1) : 1);
    }
    this.totalPossibilities = fak(possibleNodesCount) * availableSocketsCount
    let nodeIds = []
    for (let node of this.nodesArray) {
      nodeIds.push(node.id)
    }
    this.bruteForcer = new BruteForcer(nodeIds, availableSocketsCount)
    this.workingSolutions = []
    this.currentCombinationIndex = 0
    this.bruteForcing = false
  }

  bruteForceStep() {
    if (!this.bruteForcing) return false
    this.currentCombinationIndex++
    let combination = this.bruteForcer.getNextCombination()
    if (-1 === combination) return false
    if (0 !== combination) {
      this.reset()
      let index = 0
      for (let socket of this.socketsArray) {
        socket.node = this.getElement("node", combination[index])
        index++
      }
      this.solve()
      if (true === this.targets[0].state) {
        this.workingSolutions.push(combination)
      }
    }
    return true
  }


}

class BruteForcer {
  constructor(possibilities, digitCount) {
    this.digits = []
    for (let i = 0; i < digitCount; i++) {
      this.digits.push(new Digit(possibilities))
      if (0 !== i) this.digits[i-1].nextDigit = this.digits[i]
    }
    this.getNextCombination = this.getNextCombination.bind(this)
  }

  getNextCombination() {
    if (this.digits[0].increment()) {
      let combination = []
      for (let digit of this.digits) {
        let d = digit.getCurrent()
        if (d === undefined || -1 === combination.indexOf(d)) {
          combination.push(d)
        } else {
          return 0
        }
      }
      return combination
    } else {
      return -1
    }
  }
}

class Digit {
  constructor(possibilities) {
    this.possibilities = possibilities
    this.currentIndex = 0
    this.nextDigit = undefined

    this.increment = this.increment.bind(this)
  }

  getCurrent() {
    return this.possibilities[this.currentIndex]
  }

  increment() {
    this.currentIndex++
    if (this.currentIndex > this.possibilities.length) {
      this.currentIndex = 0
      if (undefined === this.nextDigit) return false
      if (!this.nextDigit.increment()) {
        return false
      }
    }
    return true
  }
}

export default Solver

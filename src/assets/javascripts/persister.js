class Persister {

  constructor(name = "myDataBase") {
    let indexedDB =
      window.indexedDB ||
      window.mozIndexedDB ||
      window.webkitIndexedDB ||
      window.msIndexedDB

    this._name = name
    this._indexedDB = indexedDB

    // bind events to component
    this._onUpgradeNeeded = this._onUpgradeNeeded.bind(this)
    this._onSuccess = this._onSuccess.bind(this)

    this._db = null
    this._request = indexedDB.open(name, 1)

    this._request.onupgradeneeded = this._onUpgradeNeeded
    this._request.onsuccess = this._onSuccess
    this._request.onerror = function(event) {
      alert("Kann auf Browser-Datenbank nicht zufreifen!")
    }
  }

  _onUpgradeNeeded() {
    // The database did not previously exist, so create object stores and indexes.
    this._db = this._request.result
    var store = this._db.createObjectStore("states", {keyPath: "name"})
    var nameIndex = store.createIndex("by_name", "name", {unique: true});
    var dataIndex = store.createIndex("by_data", "data")
  }

  _onSuccess() {
    console.log("Persister Success: ")
    console.log(this._request.result)
    this._db = this._request.result
    document._store.fetchList()
  }

  resetDB() {
    this._indexedDB.deleteDatabase(this._name)
  }

  save(data, name) {
    var tx = this._db.transaction("states", "readwrite")
    var store = tx.objectStore("states")

    var request = store.add({ name: name, data: data })

    request.onsuccess = function(event) {
      console.log("Finish")
      document._store.finishPersistState()
    }

    request.onerror = function(event) {
      alert("Fehler: Name ungültig oder bereits vorhanden!")
    }
  }

  getList() {
    var tx = this._db.transaction("states", "readwrite")
    var store = tx.objectStore("states")

    var request = store.getAll()

    request.onsuccess = function(event) {
      console.log("Got list")
      document._store.setNameList(event.target.result)
    }

    request.onerror = function(event) {
      alert("Fehler: Konnte keine Daten laden!")
    }
  }

}

export default Persister

# compiled_license

TODO: Write a description here

## Installation

Add this to your application's `shard.yml`:

```yaml
dependencies:
  compiled_license:
    github: elorest/compiled_license
```

## Usage

```crystal
require "compiled_license"
```

TODO: Write usage instructions here

## Development

TODO: Write development instructions here

## Contributing

1. Fork it ( https://github.com/elorest/compiled_license/fork )
2. Create your feature branch (git checkout -b my-new-feature)
3. Commit your changes (git commit -am 'Add some feature')
4. Push to the branch (git push origin my-new-feature)
5. Create a new Pull Request

## Contributors

- [elorest](https://github.com/elorest) Isaac Sloan - creator, maintainer
